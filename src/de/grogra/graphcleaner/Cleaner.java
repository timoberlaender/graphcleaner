package de.grogra.graphcleaner;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;
import de.grogra.xl.impl.base.EdgeIterator;
import de.grogra.xl.query.EdgeDirection;

public class Cleaner {
	static de.grogra.rgg.model.Runtime run = new de.grogra.rgg.model.Runtime();
	static List<Class> nodeWL;
	static List<Class> nodeBL;
	static List<Integer> edgeWL;

	public static void clean(Item item, Object info, Context ctx) {
		Registry r = ctx.getWorkbench().getRegistry();
		Item ite = Item.resolveItem(Workbench.current(), "/cleaner/BlackList");
		String nodeB = (String) Utils.get(ite, "nodeBlackList", "");
		System.out.println(nodeB);
		nodeBL = new ArrayList<>();
		if(nodeB.length()>0) {
			
			for (String nw : nodeB.split(",")) {
				try {
					Class x = Class.forName(nw);
					nodeBL.add(x);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		ite = Item.resolveItem(Workbench.current(), "/cleaner/WhiteList");

		String nodeW = (String) Utils.get(ite, "nodeWhiteList", "");
		System.out.println("log"+nodeW);
		nodeWL = new ArrayList<>();
		for (String nw : nodeW.split(",")) {
			try {
				Class x = Class.forName(nw);
				System.out.println(x);
				nodeWL.add(x);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String edgeW = (String) Utils.get(ite, "edgeWhiteList", "");
		edgeWL = new ArrayList<>();
		for (String ew : edgeW.split(",")) {
			edgeWL.add(Integer.parseInt(ew));
		}
		GraphManager g = r.getProjectGraph();
		Node no = g.getRoot();
		int pre = g.getGraphSize();
		check(no.getFirstEdge());
		int diff = pre - g.getGraphSize();
		System.out.println(diff);
//		ctx.getWorkbench().getLogger().log(Workbench.GUI_INFO, diff + " nodes deleted");
		ctx.getWorkbench().logGUIInfo(diff + " nodes deleted");
	}

	public static int check(Edge e) {
		int x = 0;
		for (EdgeIterator i = run.createEdgeIterator(e.getTarget(), EdgeDirection.FORWARD); i.hasEdge(); i
				.moveToNext()) {
			Edge e2 = e.getTarget().getEdgeTo((Node) i.target);
			if (edgeWL.contains(e2.getEdgeBits())) {
				x += check(e2);
			} else {
				e2.remove(null);
			}
		}
		int y = 0;
		for (Class nw : nodeWL) {
			if (nw.isInstance(e.getTarget())) {
				y = 1;
			}
		}
		for (Class nw : nodeBL) {
			if (nw.isInstance(e.getTarget())) {
				y = 0;
			}
		}
		x += y;
		if (x == 0) {
			try {
				e.remove(null);
			} catch (Exception ex) {
				System.out.println(ex);
			}
		}
		return x;
	}
}
